package com.bepensa.demprospec.Entities;

public class TiposCliente {
    private int idtipocli;
    private String nomtipocli;

    public TiposCliente(int idtipocli, String nomtipocli) {
        this.idtipocli = idtipocli;
        this.nomtipocli = nomtipocli;
    }

    public int getIdtipocli() {
        return idtipocli;
    }

    public void setIdtipocli(int idtipocli) {
        this.idtipocli = idtipocli;
    }

    public String getNomtipocli() {
        return nomtipocli;
    }

    public void setNomtipocli(String nomtipocli) {
        this.nomtipocli = nomtipocli;
    }
}
