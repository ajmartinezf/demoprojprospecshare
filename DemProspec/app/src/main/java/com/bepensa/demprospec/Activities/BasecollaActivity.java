package com.bepensa.demprospec.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Toast;

import com.bepensa.demprospec.R;
import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.StreetViewPanoramaFragment;
import com.google.android.gms.maps.model.LatLng;

public class BasecollaActivity extends AppCompatActivity implements OnStreetViewPanoramaReadyCallback {

    private String latitud;
    private String longitud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basecolla);
        try {
            StreetViewPanoramaFragment streetViewPanoramaFragment =
                    (StreetViewPanoramaFragment) getFragmentManager()
                            .findFragmentById(R.id.streetviewpanorama);
            streetViewPanoramaFragment.getStreetViewPanoramaAsync(this);
        }
        catch (Exception e)
        {
            Toast.makeText(this, e.getMessage() + " "+
                     e.getCause(), Toast.LENGTH_LONG).show();
        }




        latitud ="21.1082";
        longitud = "-89.3968";

    }

    @Override
    public void onStreetViewPanoramaReady(StreetViewPanorama streetViewPanorama) {
        streetViewPanorama.setPosition(new LatLng(Double.parseDouble(latitud),Double.parseDouble(longitud)));
    }
}
