package com.bepensa.demprospec.Entities;

public class Cliente {
    private int cuc;
    private String nombreNegocio;
    private String nombreContacto;
    private double latitud;
    private double longitud;
    private int activo;
    private TiposCliente tiposCliente;
    private Tamanio tamanio;
    private NivelSocioEconomico nivelSocioEconomico;

    public Cliente(int cuc, String nombreNegocio, String nombreContacto, double latitud, double longitud, int activo, TiposCliente tiposCliente, Tamanio tamanio, NivelSocioEconomico nivelSocioEconomico) {
        this.cuc = cuc;
        this.nombreNegocio = nombreNegocio;
        this.nombreContacto = nombreContacto;
        this.latitud = latitud;
        this.longitud = longitud;
        this.activo = activo;
        this.tiposCliente = tiposCliente;
        this.tamanio = tamanio;
        this.nivelSocioEconomico = nivelSocioEconomico;
    }

    public int getCuc() {
        return cuc;
    }

    public void setCuc(int cuc) {
        this.cuc = cuc;
    }

    public String getNombreNegocio() {
        return nombreNegocio;
    }

    public void setNombreNegocio(String nombreNegocio) {
        this.nombreNegocio = nombreNegocio;
    }

    public String getNombreContacto() {
        return nombreContacto;
    }

    public void setNombreContacto(String nombreContacto) {
        this.nombreContacto = nombreContacto;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    public TiposCliente getTiposCliente() {
        return tiposCliente;
    }

    public void setTiposCliente(TiposCliente tiposCliente) {
        this.tiposCliente = tiposCliente;
    }

    public Tamanio getTamanio() {
        return tamanio;
    }

    public void setTamanio(Tamanio tamanio) {
        this.tamanio = tamanio;
    }

    public NivelSocioEconomico getNivelSocioEconomico() {
        return nivelSocioEconomico;
    }

    public void setNivelSocioEconomico(NivelSocioEconomico nivelSocioEconomico) {
        this.nivelSocioEconomico = nivelSocioEconomico;
    }
}