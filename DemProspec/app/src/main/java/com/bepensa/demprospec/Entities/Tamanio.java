package com.bepensa.demprospec.Entities;

public class Tamanio {
    //Micro- CHICO- MEDIANO - GRANDE - EXTRAGRANDE
    private  int tamanioId;
    private String nombreDimension;

    public Tamanio(int tamanioId, String nombreDimension) {
        this.tamanioId = tamanioId;
        this.nombreDimension = nombreDimension;

    }

    public int getTamanioId() {
        return tamanioId;
    }

    public void setTamanioId(int tamanioId) {
        this.tamanioId = tamanioId;
    }

    public String getNombreDimension() {
        return nombreDimension;
    }

    public void setNombreDimension(String nombreDimension) {
        this.nombreDimension = nombreDimension;
    }
}