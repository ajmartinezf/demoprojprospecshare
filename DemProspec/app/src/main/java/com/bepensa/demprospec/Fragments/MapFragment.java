package com.bepensa.demprospec.Fragments;


import android.Manifest;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bepensa.demprospec.Controllers.MapFragmentController;
import com.bepensa.demprospec.Entities.Cliente;
import com.bepensa.demprospec.Helpers.Constants;
import com.bepensa.demprospec.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.StreetViewPanoramaView;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends Fragment implements OnMapReadyCallback, LocationListener, OnStreetViewPanoramaReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private View view;
    private GoogleMap googleMap;
    private MapView mapView;
    private StreetViewPanoramaView mapStreet;
    private StreetViewPanorama mPanorama;
    private List<Address> address;
    private Geocoder geocoder;
    private MarkerOptions markerOptions;
    private FloatingActionButton fab;
    private MapFragmentController mapFragmentController;
    private LocationManager locationManager;
    private Location location;
    private Marker marker;
    private CameraPosition cameraPosition;
    private GoogleApiClient googleApiClient;
    private GeofencingClient mGeofencingClient;
    private ArrayList<Geofence> mGeofenceList;
    private PendingIntent mGeofencePendingIntent;
    private PendingGeofenceTask mPendingGeofenceTask = PendingGeofenceTask.NONE;
    ArrayList<Cliente> _clientes;

    private enum PendingGeofenceTask {
        ADD, REMOVE, NONE
    }

    public MapFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_map, container, false);

        //fab = view.findViewById(R.id.fab);
        //fab.setOnClickListener(this);
        return view;
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_map, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        //super.onViewCreated(view, savedInstanceState);
        _clientes=mapFragmentController.obtenerClientes();
        mapView = (MapView) view.findViewById(R.id.map);
        //mapStreet = (StreetViewPanoramaView) view.findViewById(R.id.mapStreet);
        mapFragmentController = new MapFragmentController();
        if (mapView != null) {
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }

        mGeofenceList = new ArrayList<>();
        mGeofencePendingIntent = null;
        populateGeofenceList();

        //mGeofencingClient = LocationServices.getGeofencingClient(this);
//        if (mapStreet != null) {
//            mapStreet.onCreate(null);
//            mapStreet.onResume();
//            mapStreet.getStreetViewPanoramaAsync(this);
//        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;



        locationManager = (LocationManager) getContext().getSystemService(getContext().LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(getContext(),Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(),Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    Activity#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for Activity#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10000, 0, this);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 0, this);

        //Delimitar el zoom en el mapa
       /* googleMap.setMinZoomPreference(15);
        googleMap.setMaxZoomPreference(18);

        LatLng azteca = new LatLng(19.302861  , -99.150528);
        LatLng campNou = new LatLng(41.380896 , 2.12282);

        ArrayList<Cliente> _clientes=mapFragmentController.obtenerClientes();
        for (Cliente cliente: _clientes) {
            googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(cliente.getLatitud(),cliente.getLongitud())));
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(cliente.getLatitud(),cliente.getLongitud())));
        }
*/
        //googleMap.addMarker(new MarkerOptions().position(campNou).title("Camp Nou").draggable(true));
        //googleMap.addMarker(new MarkerOptions().position(azteca).title("Azteca"));

        /*CameraPosition camera = new CameraPosition.Builder().target(campNou)
                .zoom(18)
                .bearing(45)
                .tilt(90)
                .build();
        */
        /*markerOptions = new MarkerOptions();
        markerOptions.position(campNou);
        markerOptions.title("Camp Nou");
        markerOptions.draggable(true);
        markerOptions.snippet("Estadio FC Barcelona");*/
        //markerOptions.icon(BitmapDescriptorFactory.fromResource(android.R.drawable.star_on));

        //googleMap.addMarker(markerOptions);

        //CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);

        //googleMap.moveCamera(CameraUpdateFactory.newLatLng(campNou));
        //googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(camera));
        //googleMap.animateCamera(zoom);
    }

    @Override
    public void onStreetViewPanoramaReady(StreetViewPanorama streetViewPanorama) {
        LatLng azteca = new LatLng(19.302861  , -99.150528);
        //googleMap=streetViewPanorama;
        streetViewPanorama.setPosition(azteca);
    }

    @Override
    public void onLocationChanged(Location location) {
        //Toast.makeText(getContext(), "Changed ", Toast.LENGTH_SHORT).show();
        if(location != null) {

            //Buscar posiciones cercanas en la base de datos local o el servicio
            //buscarPosicionesCercanas(location);
            Toast.makeText(getContext(), "Changed " + location.getProvider(), Toast.LENGTH_SHORT).show();
            createOrUpdateMarkerLocation(location);
        }
    }



    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private void checkGps() {
        try {
            int gpsSignal = Settings.Secure.getInt(getActivity().getContentResolver(), Settings.Secure.LOCATION_MODE);
            if (gpsSignal == 0) {
                //GPS desactivado
                showAlertInfo();
            } else {
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if(location == null) {
                    location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                }
                this.location = location;
                if(this.location != null) {
                    createOrUpdateMarkerLocation(location);
                    //zoomToLocation(location);
                }
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
    }


    private void createOrUpdateMarkerLocation(Location location) {
        marker = null;
        if(marker == null) {
            int height = 100;
            int width = 100;
            BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.prospectorbepensa);
            Bitmap b=bitmapdraw.getBitmap();
            Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);

            marker = googleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromBitmap(smallMarker)).position(new LatLng(location.getLatitude(),location.getLongitude())).draggable(true));
        } else {
            marker.setPosition(new LatLng(location.getLongitude(),location.getLatitude()));
        }

        //Instantiates a new CircleOptions object +  center/radius
        CircleOptions circleOptions = new CircleOptions()
                .center( new LatLng(marker.getPosition().latitude, marker.getPosition().longitude) )
                .radius( 250 )
                .fillColor(0x40ff0000)
                .strokeColor(Color.RED)
                .strokeWidth(2);

// Get back the mutable Circle
        Circle circle = googleMap.addCircle(circleOptions);

        zoomToLocation(location);
    }


    private void showAlertInfo() {
        new AlertDialog.Builder(getContext())
                .setTitle("GPS")
                .setMessage("GPS desactivado, ¿Desea activarlo?")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Cancelar", null)
                .show();
    }

    private void zoomToLocation(Location location) {
        cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(location.getLatitude(),location.getLongitude()))
                .zoom(18)
                .bearing(0)
                .tilt(30)
                .build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    // Create GoogleApiClient instance
    private void createGoogleApi() {
        /*Log.d("ERROR EN GOOGLEAPI", "createGoogleApi()");
        if ( googleApiClient == null ) {
            googleApiClient = new GoogleApiClient.Builder( getContext() )
                    .addConnectionCallbacks( this )
                    .addOnConnectionFailedListener( this )
                    .addApi( LocationServices.API )
                    .build();
        }*/
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void populateGeofenceList() {
        for (Map.Entry<String, LatLng> entry : Constants.BAY_AREA_LANDMARKS.entrySet()) {

            mGeofenceList.add(new Geofence.Builder()
                    // Set the request ID of the geofence. This is a string to identify this
                    // geofence.
                    .setRequestId(entry.getKey())

                    // Set the circular region of this geofence.
                    .setCircularRegion(
                            entry.getValue().latitude,
                            entry.getValue().longitude,
                            Constants.GEOFENCE_RADIUS_IN_METERS
                    )

                    // Set the expiration duration of the geofence. This geofence gets automatically
                    // removed after this period of time.
                    .setExpirationDuration(Constants.GEOFENCE_EXPIRATION_IN_MILLISECONDS)

                    // Set the transition types of interest. Alerts are only generated for these
                    // transition. We track entry and exit transitions in this sample.
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                            Geofence.GEOFENCE_TRANSITION_EXIT)

                    // Create the geofence.
                    .build());
        }
    }
}