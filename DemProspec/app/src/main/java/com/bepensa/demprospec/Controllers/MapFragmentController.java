package com.bepensa.demprospec.Controllers;

import com.bepensa.demprospec.Entities.Cliente;
import com.bepensa.demprospec.Entities.NivelSocioEconomico;
import com.bepensa.demprospec.Entities.Tamanio;
import com.bepensa.demprospec.Entities.TiposCliente;

import java.util.ArrayList;
import java.util.List;

public class MapFragmentController {
    public ArrayList<Cliente> obtenerClientes() {
        ArrayList<Cliente> _clientes=new ArrayList<Cliente>();
        Cliente cliente=new Cliente(
                4037,
                "DUNOSUSA BACA",
                "MARVY BIDIRIANA TORRES KUK",
                21.1082,
                -89.3968,
                1,
                new TiposCliente(5,"Competencia"),
                new Tamanio(1,"Grande"),
                new NivelSocioEconomico(1,"A+")
        );
        _clientes.add(cliente);
        return  _clientes;
    }

    public void ObtenerListadoClientesCercanos() {
        //Manda la latitud y longiutd Actual del Propspectador/Mercadeador
        //Maximo de resultados a visualizar y zoomFactory a 15 para el area metropolitana de Mérida.
    }
}
