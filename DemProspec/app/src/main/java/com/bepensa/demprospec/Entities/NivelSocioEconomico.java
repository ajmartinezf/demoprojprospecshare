package com.bepensa.demprospec.Entities;

public class NivelSocioEconomico {
    private int idnivelsoc;
    private String nombewNivelSoc;

    public NivelSocioEconomico(int idnivelsoc, String nombewNivelSoc) {
        this.idnivelsoc = idnivelsoc;
        this.nombewNivelSoc = nombewNivelSoc;
    }

    public int getIdnivelsoc() {
        return idnivelsoc;
    }

    public void setIdnivelsoc(int idnivelsoc) {
        this.idnivelsoc = idnivelsoc;
    }

    public String getNombewNivelSoc() {
        return nombewNivelSoc;
    }

    public void setNombewNivelSoc(String nombewNivelSoc) {
        this.nombewNivelSoc = nombewNivelSoc;
    }
}
