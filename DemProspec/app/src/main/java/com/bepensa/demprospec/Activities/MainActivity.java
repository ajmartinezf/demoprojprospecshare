package com.bepensa.demprospec.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;

import com.bepensa.demprospec.Fragments.MapFragment;
import com.bepensa.demprospec.R;

public class MainActivity extends AppCompatActivity {

    Fragment currentFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        currentFragment = new MapFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer,currentFragment).commit();
    }
}